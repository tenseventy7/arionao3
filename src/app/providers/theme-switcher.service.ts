import { IonicColors } from 'ionic-angular-theme-switch';

export const defaultTheme: IonicColors = {
};

// https://github.com/ionic-team/ionic/blob/master/core/src/themes/test/css-variables/css/dark.css
export const darkTheme: IonicColors = {
	primary: '#dc4934',
	secondary: '#76f8c5',
	tertiary: '#ffbf6e',
	success: '#76f8c5',
	warning: '#ffff51',
	danger: '#de492c',
	'ion-background-color': '#121212',
  'ion-text-color': '#ffffff',
  'ion-card-md-color': '#272727',
  'ion-bottom-md-color': '#1E1E1E',
  light: '#595959',
 	medium: '#a0a0a0',
	'ion-border-color': '#222222',
	'ion-color-step-50': '#1e1e1e',
	'ion-color-step-100': '#2a2a2a',
	'ion-color-step-150': '#363636',
	'ion-color-step-200': '#414141',
	'ion-color-step-250': '#4d4d4d',
	'ion-color-step-300': '#595959',
	'ion-color-step-350': '#656565',
	'ion-color-step-400': '#717171',
	'ion-color-step-450': '#7d7d7d',
	'ion-color-step-500': '#898989',
	'ion-color-step-550': '#949494',
	'ion-color-step-600': '#a0a0a0',
	'ion-color-step-650': '#acacac',
	'ion-color-step-700': '#b8b8b8',
	'ion-color-step-750': '#c4c4c4',
	'ion-color-step-800': '#d0d0d0',
	'ion-color-step-850': '#dbdbdb',
	'ion-color-step-900': '#e7e7e7',
	'ion-color-step-950': '#f3f3f3',
};

// https://github.com/ionic-team/ionic/blob/master/core/src/themes/test/css-variables/css/oceanic.css
export const oceanicTheme: IonicColors = {
  primary: '#549ee7',
  secondary: '#5fb3b3',
  tertiary: '#fac863',
  success: '#90d089',
  warning: '#f99157',
  danger: '#ec5f67',
  light: '#d8dee9',
  medium: '#65737e',
  dark: '#1b2b34',

  'ion-background-color': '#1b2b34',
  'ion-text-color': '#fff',

  /* Component Colors */
  'ion-backdrop-color': '#1b2b34',
  'ion-overlay-background-color': '#142129',

  'ion-border-color': '#1b2b34',
  'ion-box-shadow-color': '#000',

  'ion-item-background': '#343d46',
  'ion-item-background-activated': '#232b34'
};

// https://github.com/ionic-team/ionic/blob/master/core/src/themes/test/css-variables/css/vibrant.css
export const vibrantTheme: IonicColors = {
  primary: '#ff7f50',
  secondary: '#17deee',
  tertiary: '#ff4162',
  success: '#39ff14',
  warning: '#ffce00',
  danger: '#f04141',
  light: '#f4f5f8',
  medium: '#989aa2',
  dark: '#222428',

  'ion-background-color': '#778899',

  /* Component Colors */
  'ion-backdrop-color': '#556677',
  'ion-overlay-background-color': '#667788',

  'ion-border-color': '#5bff76',
  'ion-box-shadow-color': '#000',

  'ion-item-background': '#667788',
  'ion-item-border-color': '#5bff76'
};